#!/bin/bash

CURDIR="$(dirname ${BASH_SOURCE[0]})"
: ${CXX:=c++}

printf "Building with compiler: '%s'\n" "${CXX}"

time \
${CXX} -g -O0 -std=c++11 -Werror -Wall \
  -o vulkan \
  -I/usr/include \
  -I "${CURDIR}/external/glfw3/include" \
  -L "${CURDIR}/external/vulkan" \
  -l ":libvulkan.so" \
  -Wl,-rpath='$ORIGIN/external/vulkan' \
  -lX11 \
  main.cpp \
  "${CURDIR}/external/glfw3/libglfw3.a"
RESULT=$?

if [[ $RESULT == 0 ]]; then
  echo 'Successfully built "vulkan"'
else
  echo 'Failed to build "vulkan"'
fi
echo ''

exit $RESULT
