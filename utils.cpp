#include <assert.h>
#include <cstdlib>
#include <ctime>
#include <stdarg.h>
#include <stdio.h>
#include <vulkan/vulkan.h>

#define ARRAY_LENGTH(arr) (sizeof(arr) / sizeof(*arr))

#ifdef __GNUC__
#define UNUSED __attribute__((unused))
#else
#define UNUSED
#endif

void
log(const char *func, size_t line, const char *msg, ...)
{
   va_list args;
   va_start(args, msg);
   char formatted_msg[256];
   vsnprintf(formatted_msg, ARRAY_LENGTH(formatted_msg), msg, args);
   va_end(args);

   printf("[%s():%lu] ::  %s\n", func, line, formatted_msg);
}

bool
logAndAssert(bool predicate,
             const char *predicate_string,
             const char *func,
             size_t line,
             const char *msg,
             ...)
{
   if (predicate)
      return true;

   va_list args;
   va_start(args, msg);
   char formatted_msg[256];
   vsnprintf(formatted_msg, ARRAY_LENGTH(formatted_msg), msg, args);
   va_end(args);

   printf("ASSERT [%s():%lu] ::  Assertion \"%s\" failed with message:  %s\n", func, line,
          predicate_string, formatted_msg);

#ifndef NDEBUG
   fflush(stdout);
   exit(1);
#endif

   return false;
}

bool
logAndAssertVulkan(VkResult result, const char *func, size_t line, const char *msg, ...)
{
   if (result == VK_SUCCESS)
      return true;

   va_list args;
   va_start(args, msg);
   char formatted_msg[256];
   vsnprintf(formatted_msg, ARRAY_LENGTH(formatted_msg), msg, args);
   va_end(args);

   printf("VK_ASSERT [%s():%lu] ::  Vulkan command failed with error %d and "
          "message:  %s\n",
          func, line, result, formatted_msg);

#ifndef NDEBUG
   fflush(stdout);
   exit(1);
#endif

   return false;
}

#define LOG(msg, ...) log(__func__, __LINE__, msg, ##__VA_ARGS__)

#define ASSERT(predicate, msg, ...)                                                      \
   logAndAssert(predicate, #predicate, __func__, __LINE__, msg, ##__VA_ARGS__)

#define VK_ASSERT(vkresult, msg, ...)                                                    \
   logAndAssertVulkan(vkresult, __func__, __LINE__, msg, ##__VA_ARGS__)

struct Timer {
   struct timespec t;
};

UNUSED struct Timer
timer_start()
{
   struct timespec start;
   int ret;

   ret = clock_getres(CLOCK_MONOTONIC_RAW, &start);
   assert(!ret);
   assert(start.tv_sec == 0);
   assert(start.tv_nsec == 1);

   ret = clock_gettime(CLOCK_MONOTONIC_RAW, &start);
   assert(!ret);

   return (struct Timer){
      .t = start,
   };
}

UNUSED uint64_t
timer_stop(struct Timer *timer)
{
   struct timespec stop;
   clock_gettime(CLOCK_MONOTONIC_RAW, &stop);

   assert(stop.tv_sec > timer->t.tv_sec ||
          (stop.tv_sec == timer->t.tv_sec && stop.tv_nsec >= timer->t.tv_nsec));
   uint64_t diff_us = ((int64_t)stop.tv_sec - (int64_t)timer->t.tv_sec) * 1000000LL +
                      ((int64_t)stop.tv_nsec - timer->t.tv_nsec) / 1000LL;

   return diff_us;
}

UNUSED uint64_t
timer_stop_and_log(struct Timer *timer, const char *description)
{
   uint64_t diff_us = timer_stop(timer);
   LOG("timer '%s': %lu us", description, diff_us);
   return diff_us;
}
