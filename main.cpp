#include <stdio.h>
#include <string.h>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

#include <GLFW/glfw3.h>

#include "utils.cpp"

#define ALLOCATE_TYPE(type, count) (type *)(malloc(sizeof(type) * count))
#define ZALLOCATE_TYPE(type, count) (type *)(calloc(count, sizeof(type)))
#define ARRAY_LENGTH(arr) (sizeof(arr) / sizeof(*arr))

// define to take control of allocation
VkAllocationCallbacks *global_allocator = NULL;

static void glfw_error_callback(int error_code, const char *msg) {
   LOG("GLFW ERROR(%d): %s", error_code, msg);
}
static void init_glfw() {
   glfwInit();
   glfwSetErrorCallback(glfw_error_callback);

   assert(glfwVulkanSupported() == GLFW_TRUE);
}

VkInstance
create_vk_instance(bool enable_validation = true)
{
   VkApplicationInfo app_info = {};
   app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
   app_info.pApplicationName = "Vulkan example";
   app_info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
   app_info.pEngineName = "'the' engine";
   app_info.engineVersion = VK_MAKE_VERSION(1, 0, 0);
   app_info.apiVersion = VK_API_VERSION_1_3;

   uint32_t enabled_layer_count = 0;
   const char *enabled_layer_names[4];

   uint32_t layer_property_count;
   vkEnumerateInstanceLayerProperties(&layer_property_count, NULL);
   if (enable_validation && layer_property_count > 0) {
      // make sure we have the validation layer available
      VkLayerProperties *layer_props =
         ALLOCATE_TYPE(VkLayerProperties, layer_property_count);
      vkEnumerateInstanceLayerProperties(&layer_property_count, layer_props);

#define validation_layer_name "VK_LAYER_KHRONOS_validation"
      for (uint32_t i = 0; i < layer_property_count; i++) {
         auto *prop = &layer_props[i];
         if (!strcmp(prop->layerName, validation_layer_name)) {
            LOG("Enabling Vulkan Validation Layer");
            enabled_layer_names[enabled_layer_count++] = validation_layer_name;
            break;
         }
      }
#undef validation_layer_name

      free(layer_props);
   }

   // TODO(ryan): use glfwGetRequiredInstanceExtensions()

   // enable our own extensions
   const char *extra_extensions[] = {
      "VK_KHR_surface",
      "VK_KHR_xcb_surface",
      VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME,
   };

   uint32_t extensions_count = 0;
   const char **extensions = NULL;
   if (ARRAY_LENGTH(extra_extensions)) {
      extensions = ALLOCATE_TYPE(__typeof__(*extensions), ARRAY_LENGTH(extra_extensions));

      for (uint32_t i = 0; i < ARRAY_LENGTH(extra_extensions); i++)
         extensions[extensions_count++] = extra_extensions[i];
   }

   VkInstanceCreateInfo instance_create_info = {
      .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
      .pApplicationInfo = &app_info,
      .enabledLayerCount = enabled_layer_count,
      .ppEnabledLayerNames = enabled_layer_names,
      .enabledExtensionCount = extensions_count,
      .ppEnabledExtensionNames = extensions,
   };

   VkInstance instance;
   VkResult result = vkCreateInstance(&instance_create_info, NULL, &instance);

   if (VK_ASSERT(result, "vkCreateInstance failed"))
      return instance;
   else
      return NULL;
}

static const char *
get_vk_physical_device_type_string(VkPhysicalDeviceType type)
{
   switch (type) {
   case VK_PHYSICAL_DEVICE_TYPE_CPU:
      return "cpu";
   case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
      return "igpu";
   case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
      return "dgpu";
   default:
      return "unknown";
   }
}

VkPhysicalDevice
select_physical_device(VkInstance instance)
{
#if 0
   // not necessary, but here to demonstate group enumeration
   uint32_t group_count;
   vkEnumeratePhysicalDeviceGroups(instance, &group_count, NULL);
   LOG("Number of device groups: %u", group_count);

   VkPhysicalDeviceGroupProperties *pdevgroups = ALLOCATE_TYPE(VkPhysicalDeviceGroupProperties, group_count);
   vkEnumeratePhysicalDeviceGroups(instance, &group_count, pdevgroups);
   for (uint32_t i = 0; i < group_count; i++) {
      LOG("group %u has %u devices", i, pdevgroups[i].physicalDeviceCount);
   }
   free(pdevgroups);
#endif

   uint32_t pcount;
   vkEnumeratePhysicalDevices(instance, &pcount, NULL);
   ASSERT(pcount, "No vulkan-compatible physical devices found.");

   VkPhysicalDevice *pdevices = ALLOCATE_TYPE(VkPhysicalDevice, pcount);
   VkResult result = vkEnumeratePhysicalDevices(instance, &pcount, pdevices);
   VK_ASSERT(result, "Failed to enumerate physical devices");
   LOG("Number of physical devices %u", pcount);

   // only accept if it supports a graphics queue family
   VkPhysicalDevice selected_pdevice = 0;

   VkPhysicalDeviceProperties pprops;
   for (uint32_t i = 0; i < pcount; i++) {
      vkGetPhysicalDeviceProperties(pdevices[i], &pprops);
      if (pprops.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU ||
          pprops.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU) {
         selected_pdevice = pdevices[i];
         LOG("Selected physical device #%d", i);
         LOG("  device name: %s", pprops.deviceName);
         LOG("  device type: %s", get_vk_physical_device_type_string(pprops.deviceType));
         LOG("  device api version: %d.%d.%d", VK_VERSION_MAJOR(pprops.apiVersion),
             VK_VERSION_MINOR(pprops.apiVersion), VK_VERSION_PATCH(pprops.apiVersion));
         break;
      }
   }
   free(pdevices);

   ASSERT(selected_pdevice,
          "Failed to find a suitable physical device (considered %u choices)", pcount);
   return selected_pdevice;
}

struct QueueFamily {
   uint32_t index;
   uint32_t queue_count;
   VkQueueFlags flags;
};

QueueFamily
select_queue_family(VkPhysicalDevice physical_device)
{
   uint32_t count;
   vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &count, NULL);

   VkQueueFamilyProperties *props = ALLOCATE_TYPE(VkQueueFamilyProperties, count);
   vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &count, props);

   bool found_valid = false;
   QueueFamily queue_family = {};
   for (uint32_t i = 0; i < count; i++) {
      bool has_graphics = props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT;

      LOG("Queue Family #%d", i);
      LOG("  queue count: %d", props[i].queueCount);
      LOG("  queue flags: %d (graphics: %s)", props[i].queueFlags,
          has_graphics ? "yes" : "no");

      if (!found_valid && has_graphics) {
         found_valid = true;
         queue_family.index = i;
         queue_family.queue_count = props[i].queueCount;
         queue_family.flags = props[i].queueFlags;
      }
   }

   ASSERT(found_valid,
          "Failed to identify a valid queue family containing graphics+present "
          "support");
   return queue_family;
}

VkDevice
create_device(VkPhysicalDevice physical_device, QueueFamily queue_family)
{
   float priority = 1.f;
   VkDeviceQueueCreateInfo queue_create_info = {
      .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
      .queueFamilyIndex = queue_family.index,
      .queueCount = 1,
      .pQueuePriorities = &priority,
   };

   VkPhysicalDeviceFeatures device_features = {};

   const char *extension_names[] = {};

   VkDeviceCreateInfo device_create_info = {
      .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
      .queueCreateInfoCount = 1,
      .pQueueCreateInfos = &queue_create_info,
      .enabledLayerCount = 0,
      .enabledExtensionCount = ARRAY_LENGTH(extension_names),
      .ppEnabledExtensionNames = extension_names,
      .pEnabledFeatures = &device_features,
   };

   VkDevice device;
   VkResult res = vkCreateDevice(physical_device, &device_create_info, NULL, &device);

   VK_ASSERT(res, "Failed to create logical device");
   ASSERT(device, "Failed to create logical device");
   return device;
}

struct SwapchainInfo {
   VkPresentModeKHR present_mode;
   uint32_t image_count;
   uint32_t width;
   uint32_t height;
   VkSwapchainKHR swapchain;
};

struct RendererState {
   VkInstance instance;
   VkPhysicalDevice physical_device;
   QueueFamily queue_family;
   VkDevice device;
   VkQueue graphics_queue;

   SwapchainInfo swapchain_info;

} renderer;

void
get_queues(RendererState *renderer)
{
   uint32_t queue_index = 0;
   VkQueue queue;
   vkGetDeviceQueue(renderer->device, renderer->queue_family.index, queue_index, &queue);

   ASSERT(queue, "Failed to create queue");
   renderer->graphics_queue = queue;
}

void
init_vulkan(RendererState *renderer)
{
   renderer->instance = create_vk_instance();
   renderer->physical_device = select_physical_device(renderer->instance);
   renderer->queue_family = select_queue_family(renderer->physical_device);
   renderer->device = create_device(renderer->physical_device, renderer->queue_family);

   get_queues(renderer);
   LOG("Finished initializing vulkan");
   LOG("============================");
}

void
cleanup_vulkan(RendererState *renderer)
{
   vkDestroyDevice(renderer->device, global_allocator);
   vkDestroyInstance(renderer->instance, global_allocator); // must be last
}

static const char *
present_mode_to_string(VkPresentModeKHR mode) {
   #define CASE(mode) case mode:  return #mode
   switch (mode) {
      CASE(VK_PRESENT_MODE_IMMEDIATE_KHR);
      CASE(VK_PRESENT_MODE_MAILBOX_KHR);
      CASE(VK_PRESENT_MODE_FIFO_KHR);
      CASE(VK_PRESENT_MODE_FIFO_RELAXED_KHR);
      CASE(VK_PRESENT_MODE_SHARED_DEMAND_REFRESH_KHR);
      CASE(VK_PRESENT_MODE_SHARED_CONTINUOUS_REFRESH_KHR);
      default:  return "unknown";
   }
   #undef CASE
}

struct Window {
   GLFWwindow *glfw_window;
   VkSurfaceKHR surface;
   VkSwapchainKHR swapchain;
};

Window
create_window(RendererState *renderer, uint32_t width, uint32_t height)
{
   glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
   GLFWwindow *glfw_window = glfwCreateWindow(width, height, "vulkan", NULL, NULL);
   assert(glfw_window);

   VkSurfaceKHR surface;
   glfwCreateWindowSurface(renderer->instance, glfw_window, NULL, &surface);

   {
      VkPresentModeKHR present_modes[8];
      uint32_t mode_count = ARRAY_LENGTH(present_modes);
      vkGetPhysicalDeviceSurfacePresentModesKHR(renderer->physical_device, surface, &mode_count, present_modes);

      LOG("Supported presentation modes:");
      for (uint32_t i = 0; i < mode_count; i++) {
         LOG("  - %s", present_mode_to_string(present_modes[i]));
      }
   }

   VkSwapchainKHR swapchain = NULL;
#if 0
   VkSwapchainCreateInfoKHR swapchain_create_info = {
      .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
      .surface = surface,
    VkStructureType                  sType;
    const void*                      pNext;
    VkSwapchainCreateFlagsKHR        flags;
    VkSurfaceKHR                     surface;
    uint32_t                         minImageCount;
    VkFormat                         imageFormat;
    VkColorSpaceKHR                  imageColorSpace;
    VkExtent2D                       imageExtent;
    uint32_t                         imageArrayLayers;
    VkImageUsageFlags                imageUsage;
    VkSharingMode                    imageSharingMode;
    uint32_t                         queueFamilyIndexCount;
    const uint32_t*                  pQueueFamilyIndices;
    VkSurfaceTransformFlagBitsKHR    preTransform;
    VkCompositeAlphaFlagBitsKHR      compositeAlpha;
    VkPresentModeKHR                 presentMode;
    VkBool32                         clipped;
    VkSwapchainKHR                   oldSwapchain;
   };
   VkResult result = vkCreateSwapchainKHR(renderer->device, &swapchain_create_info, NULL, &swapchain);
   VK_ASSERT(result, "Failed to create swapchain");
 #endif

   Window window = {
      .glfw_window = glfw_window,
      .surface = surface,
      .swapchain = swapchain,
   };

   LOG("Finished initializing window");
   LOG("============================");
   return window;
}

static void cleanup_window(RendererState *renderer, Window *window) {
   vkDestroySurfaceKHR(renderer->instance, window->surface, NULL);
   glfwDestroyWindow(window->glfw_window);
}

UNUSED static void
run_render_loop(RendererState *renderer, Window *window) {
   while (true) {
      glfwPollEvents();

      if (glfwWindowShouldClose(window->glfw_window) ||
          glfwGetKey(window->glfw_window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
         break;
      }

      // Draw
   }
}

#define RUN_WINDOW_LOOP

int
main()
{
   RendererState renderer = {};
   init_vulkan(&renderer);

   init_glfw();
   Window window = create_window(&renderer, 500, 500);

   // SAMPLE STUFF

   // END SAMPLE STUFF

   // run_render_loop(&renderer, &window);

   cleanup_window(&renderer, &window);
   cleanup_vulkan(&renderer);

   return 0;
}
